<?php

namespace App\Services\Helper;

use Morilog\Jalali\Jalalian;

class Time
{
    public static function daykeyToTimestamp(string $daykey, string $timekey = null): int
    {
        $year   = substr($daykey, 0, 4);
        $month  = substr($daykey, 4, 2);
        $day    = substr($daykey, 6, 2);
        $hour   = 0;
        $min    = 0;

        if ($timekey) {
            $hour   = self::timekeyToArray($timekey)['hour'];
            $min    = self::timekeyToArray($timekey)['min'];
        }

        $dateInstance = new Jalalian($year, $month, $day, $hour, $min, $sec = 0);
        return $dateInstance->getTimestamp();
    }

    public static function daykeyToString(string $daykey): string
    {
        $timestamp = self::daykeyToTimestamp($daykey);
        return self::timestampToString($timestamp);
    }

    public static function daykeyToRelative(string $daykey): string
    {
        $timestamp = self::daykeyToTimestamp($daykey);
        return self::timestampToRelative($timestamp);
    }

    public static function daykeyToArray(string $daykey): array
    {
        return [
            "year"  => substr($daykey, 0, 4),
            "month" => substr($daykey, 4, 2),
            "day"   => substr($daykey, 6, 2)
        ];
    }

    public static function timekeyToArray(string $timekey): array
    {
        return [
            "hour"  => substr($timekey, 0, 2),
            "min"   => substr($timekey, 2, 2)
        ];
    }

    public static function timekeyToString(string $timekey): string
    {
        $time = self::timekeyToArray($timekey);
        return $time['hour'] . ':' . $time['min'];
    }

    public static function timestampToString(int $timestamp): string
    {
        return Jalalian::forge($timestamp)->format(env('DATE_FORMAT'));
    }

    public static function timestampToRelative(int $timestamp): string
    {
        return Jalalian::forge($timestamp)->ago();
    }

    public static function timestampToDaykey(int $timestamp, bool $sep = false): string
    {
        if ($sep) return Jalalian::forge($timestamp)->format('%Y/%m/%d');
        return Jalalian::forge($timestamp)->format('%Y%m%d');
    }

    public static function timestampToTimekey(int $timestamp, bool $sep = false): string
    {
        if ($sep) return Jalalian::forge($timestamp)->format('H:i');
        return Jalalian::forge($timestamp)->format('Hi');
    }
}
