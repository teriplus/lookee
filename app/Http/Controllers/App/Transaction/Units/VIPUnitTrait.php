<?php

namespace App\Http\Controllers\App\Transaction\Units;

use Carbon\Carbon;
use Illuminate\Http\Request;

trait VIPUnitTrait
{
    public function oneMonthVIP(Request $request)
    {
        $vip_expire_at = $request->auth->vip_expire_at ?? 0;

        $request->auth->update([
            "vip_status"    => \App\Models\User::$vip_status['ACTIVE'],
            "vip_expire_at" => strtotime('+30 days', $vip_expire_at)
        ]);
    }

    public function threeMothsVIP(Request $request)
    {
        $vip_expire_at = $request->auth->vip_expire_at ?? 0;

        $request->auth->update([
            "vip_status"    => \App\Models\User::$vip_status['ACTIVE'],
            "vip_expire_at" => strtotime('+90 days', $vip_expire_at)
        ]);
    }
}
