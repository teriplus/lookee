<?php

namespace Database\Factories;

use Carbon\Carbon;
use App\Models\EventUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EventUser::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'       => function () {
                $users_id = \App\Models\User::pluck('id');
                return $users_id[rand(0, $users_id->count() - 1)];
            },
            'event_id'      => function () {
                $event_id = \App\Models\Event::pluck('id');
                return $event_id[rand(0, $event_id->count() - 1)];
            },
            'created_at'    => Carbon::now()->timestamp,
        ];
    }
}
