<?php

namespace App\Models\ACL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RoleUser extends Model
{
    // use HasFactory;
    
    protected $table = "role_user";

    public $timestamps = false;

    protected $guarded = [];
}
