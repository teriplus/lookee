@component('mail::message')
# Email Verification

Dear , {{ $name }}

@component('mail::button', ['url' => $link])
    VERIFY NOW
@endcomponent

thanks , Lookee
@endcomponent
