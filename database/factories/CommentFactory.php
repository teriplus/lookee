<?php

namespace Database\Factories;

use App\Models\Event;
use App\Models\Comment;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'           => function () {
                $users_id = \App\Models\User::pluck('id');
                return $users_id[rand(0, $users_id->count() - 1)];
            },
            'commentable_type'  => function () {
                $types = [
                    Comment::class, Event::class
                ];
                return $types[rand(0, sizeof($types) - 1)];;
            },
            'commentable_id'    => rand(0, 10),
            'body'              => $this->faker->text(80),
            'created_at'        => Carbon::now()->timestamp,
        ];
    }
}
