<?php

namespace App\Services\Payment;

use Illuminate\Http\Request;

interface GetwayInterface
{
    const TRANSACTION_SUCCESS = "success";
    const TRANSACTION_ONPAY = "onPay";
    const TRANSACTION_FAIL = "fail";

    public static function getSupportTransaction();
    public function pay(array $options): void;
    public function verify(Request $request): array;
}
