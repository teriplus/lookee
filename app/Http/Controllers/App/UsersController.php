<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function checkAuth(Request $request)
    {
        return response()->json([
            'status' => 'success',
            'name' => $request->auth->name
        ], 200);
    }
}
