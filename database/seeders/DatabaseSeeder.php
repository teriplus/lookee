<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Event;
use App\Models\EventUser;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Create initial user 
         *
         * @return User
         */
        User::factory(1)->create();


        /**
         * Create event directly(systemic) 
         *
         * @return Event
         */
        Event::factory(1)->directly()->create()->each(function ($event) {
            EventUser::create([
                'user_id'    => $event->user_id,
                'event_id'   => $event->id,
                'created_at' => time()
            ]);
        });;


        /**
         * Create comment
         *
         * @return Comment
         */
        // Comment::factory(100)->create();


        /**
         * Create EventUser
         *
         * @return EventUser
         */
        // EventUser::factory(50)->create();
    }
}
