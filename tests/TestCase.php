<?php

use Laravel\Lumen\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{

    protected function getToken(): string
    {
        return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJsdW1lbi1qd3QiLCJzdWIiOjEsImlhdCI6MTYzNzkxODkzNCwiZXhwIjoxNjM4NTIzNzM0fQ.vvfXlKbKR_rWsvPOVzUqAICVrpOr8nZLxGgyt_6gFd8";
    }

    protected function getContent(object $response, $key = null)
    {
        return is_null($key) ? json_decode($this->response->content(), true)
            : json_decode($this->response->content(), true)[$key];
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        if (!defined('LARAVEL_START')) {
            define('LARAVEL_START', microtime(true));
        }

        return require __DIR__ . '/../bootstrap/app.php';
    }
}
