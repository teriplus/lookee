<?php

namespace App\Services\ACL\traits;

use App\Models\User;
use App\Services\ACL\ACL;

trait Checkable
{
    protected function check(array $permissions,User $auth)
    {
        if (!ACL::hasPermissions($permissions, $auth))
            return abort(403);
    }
}
