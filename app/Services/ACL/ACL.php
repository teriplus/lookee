<?php

namespace App\Services\ACL;

use App\Models\User;
use App\Models\ACL\Role;
use App\Services\ACL\contracts\ACLInterface;

/**
 * 
 * 1.make permission, role, role_user, permission_role
 * 
 * 2. user model
 * 
 *     public function roles()
 *    {
 *        return $this->belongsToMany(Role::class);
 *    }
 * 
 */

class ACL implements ACLInterface
{
    public static function giveRoles(array $roles, User $user)
    {
        try {
            $roles = self::getAllRoles($roles);
            $user->roles()->syncWithoutDetaching($roles);
            return true;
        } catch (\Throwable $th) {
            throw new \Exception($th);
        }
    }

    public static function takeOverRoles(array $roles, User $user)
    {
        try {
            $roles = self::getAllRoles($roles);
            $user->roles()->detach($roles);
            return true;
        } catch (\Throwable $th) {
            throw new \Exception($th);
        }
    }

    public static function refreshRoles(array $roles, User $user)
    {
        try {
            $roles = self::getAllRoles($roles);
            $user->roles()->sync($roles);
            return true;
        } catch (\Throwable $th) {
            throw new \Exception($th);
        }
    }

    public static function hasRoles($roles, User $user): bool
    {
        foreach ($roles as $role) {
            $chech = $user->roles->contains('name', $role);
            if ($chech == false) {
                return false;
            }
        }
        return true;
    }

    public static function hasAnyRoles($roles, User $user): bool
    {
        foreach ($roles as $role) {
            $chech = $user->roles->contains('name', $role);
            if ($chech == true) {
                return true;
            }
        }
        return false;
    }

    public static function hasPermissions($assert_permissions, User $user)
    {
        $user_permissions = [];
        foreach ($user->roles as $role) {
            foreach ($role->permissions as $rp) {
                array_push($user_permissions, $rp['name']);
            }
        }
        foreach ($assert_permissions as $assert_permission) {
            if (in_array($assert_permission, $user_permissions) == false)
                return false;
        }
        return true;
    }

    public static function userPermissions(User $user)
    {
        $user_permissions = [];
        foreach ($user->roles as $role) {
            foreach ($role->permissions as $rp) {
                array_push($user_permissions, $rp['name']);
            }
        }
        return array_unique($user_permissions);
    }

    private static function getAllRoles($roles)
    {
        return Role::whereIn('name', $roles)->get();
    }
}
