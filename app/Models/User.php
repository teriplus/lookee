<?php

namespace App\Models;

use App\Models\Comment;
use App\Models\Payment;
use App\Models\ACL\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Model
{
    use HasFactory;

    protected $table = 'users';

    public $timestamps = false;

    protected $guarded = [];

    protected $hidden = ['pivot'];

    public static $showable = [
        'id', 'name', 'avatar', 'birth_day',
        'gender', 'email', 'phone_number', 'bio',
        'wallet', 'status', 'vip_status', 'score',
        'vip_expire_at', 'telegram_id', 'instagram_id',
        'email_verified_at', 'phone_number_verified_at'
    ];

    public static $updatable = [
        'name', 'birth_day', 'gender', 'bio', 'telegram_id', 'instagram_id',
    ];

    public static $status = [
        'INACTIVE' => 0,
        'ACTIVE'   => 1,
        'DELETED'  => 2,
        'PENDING'  => 3,
    ];

    public static $vip_status = [
        'INACTIVE' => 0,
        'ACTIVE'   => 1,
    ];

    public function events()
    {
        return $this->hasMany(Event::class)->latest();
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function in_events()
    {
        return $this->belongsToMany(Event::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
