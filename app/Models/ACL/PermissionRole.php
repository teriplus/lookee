<?php

namespace App\Models\ACL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PermissionRole extends Model
{
    // use HasFactory;
    
    protected $table = "permission_role";

    public $timestamps = false;

    protected $guarded = [];
}
