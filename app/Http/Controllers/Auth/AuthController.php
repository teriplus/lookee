<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use App\Events\UserRegisterEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Create a new token.
     * 
     * @param  \App\Models\User   $user
     * @return string
     */
    protected function jwt(User $user)
    {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => strtotime('+7 days') // Expiration time
        ];

        // As you can see we are passing `APP_KEY` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('APP_KEY'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\User   $user 
     * @return mixed
     */
    public function authenticate(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email'     => 'required|email',
                'password'  => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'failed',
                'message' => $validator->errors()->first()
            ], 422);
        }

        $user = User::where('email', $this->request->input('email'))->first();

        if (!$user) {
            return response()->json([
                'message' => 'ایمیل در سیستم وجود ندارد',
                'status'  => 'failed'
            ], 400);
        }

        // Verify the password and generate the token
        if (Hash::check($this->request->input('password'), $user->password)) {
            return response()->json([
                'token' => $this->jwt($user),
                'name'  => $user->name,
                'status' => 'success'
            ], 200);
        }

        // Bad Request response
        return response()->json([
            'message' => 'ایمیل یا رمزعبور اشتباه است',
            'status'  => 'failed'
        ], 400);
    }

    public function register(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'name'      => 'required',
                'email'     => 'required|email|unique:users',
                'password'  => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'failed',
                'message' => $validator->errors()->first()
            ], 422);
        }

        $user = User::create([
            "email" => $request->input('email'),
            "name"  => $request->input('name'),
            "password" => app('hash')->make($request->input('password')),
            "created_at" => Carbon::now()->timestamp,
        ]);

        event(new UserRegisterEvent($user));

        return response()->json([
            'token' => $this->jwt($user),
            'name'  => $user->name,
            'status' => 'success'
        ], 200);
    }

    public function forget(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|exists:users',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'failed',
                'message' => $validator->errors()->first()
            ], 422);
        }

        return response()->json([
            'message' => 'this part should be complete [AuthController forget method]', // TODO
            'status'  => 'success'
        ], 200);
    }

    public function reset(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'current_password'  => 'required',
                'new_password'      => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'failed',
                'message' => $validator->errors()->first()
            ], 422);
        }

        $checkPwd = Hash::check($this->request->input('current_password'), $request->auth->password);

        if (!$checkPwd) {
            return response()->json([
                'message' => 'رمزعبور اشتباه است',
                'status'  => 'failed'
            ], 403);
        }

        try {
            $user = User::findOrFail($request->auth->id);
            $user->password = app('hash')->make($request->input('new_password'));
            $user->save();

            return response()->json([
                'message' => 'رمزعبور با موفقیت تغییر کرد',
                'status'  => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'در انجام عملیات مشکلی پیش آمده,لطفا مجددا تلاش کنید',
                'status'  => 'failed'
            ], 500);
        }
    }
}
