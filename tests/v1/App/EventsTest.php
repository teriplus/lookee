<?php

use App\Models\Event;

class EventsTest extends TestCase
{
    /**
     *  @test 
     *  @depends AuthTest::can_login
     */
    public function can_store_event(array $auth)
    {
        $string_id = 'f_' . bin2hex(openssl_random_pseudo_bytes(6));

        $response = $this->json(
            'POST',
            'v1/event/store',
            Event::factory()->unDirectly()->make()->toArray(),
            [
                'token' => $auth['token']
            ]
        );
        $this->assertEquals($this->response->status(), 201);
        return Event::latest()->first();
    }

    /**
     *  @test 
     *  @depends AuthTest::can_login
     *  @depends can_store_event
     */
    public function can_see_event_members(array $auth, Event $event)
    {
        $response = $this->json(
            'POST',
            'v1/event/members',
            ['id'       => $event->id],
            ['token'    => $auth['token']]
        );
        $this->assertEquals($this->response->status(), 200);
    }

    /**
     *  @test 
     *  @depends can_store_event
     */
    public function can_load_event(Event $event)
    {
        $response = $this->json(
            'POST',
            'v1/event',
            [
                "id" => $event->id,
            ]
        );
        $this->assertEquals($this->response->status(), 200);
    }

    /**
     *  @test 
     */
    public function can_load_events()
    {
        $response = $this->json(
            'POST',
            'v1/events/1'
        );
        $this->assertEquals($this->response->status(), 200);
    }

    /**
     *  @test 
     *  @depends AuthTest::can_login
     */
    public function can_load_my_events(array $auth)
    {
        $response = $this->json(
            'POST',
            'v1/my-events',
            [],
            [
                'token' => $auth['token']
            ]
        );
        $this->assertEquals($this->response->status(), 200);
    }

    /**
     *  @test 
     *  @depends AuthTest::can_login
     *  @depends can_store_event
     */
    public function can_update_event(array $auth, Event $event)
    {
        $response = $this->json(
            'POST',
            'v1/event/update',
            array_merge(['id' => $event->id], Event::factory()->unDirectly()->make()->toArray()),
            [
                'token' => $auth['token']
            ]
        );
        $this->assertEquals($this->response->status(), 200);
    }

    /**
     *  @test 
     *  @depends AuthTest::can_login
     *  @depends can_store_event
     */
    public function can_leave_event(array $auth, Event $event)
    {
        $response = $this->json(
            'POST',
            'v1/event/leave',
            ['id'       => $event->id],
            ['token'    => $auth['token']]
        );
        $this->assertEquals($this->response->status(), 200);
        $this->notSeeInDatabase('event_user', [
            'event_id'  => $event->id,
            'user_id'   => $auth['user']->id,
        ]);
    }

    /**
     *  @test 
     *  @depends AuthTest::can_login
     *  @depends can_store_event
     */
    public function can_join_event(array $auth, Event $event)
    {
        $response = $this->json(
            'POST',
            'v1/event/join',
            ['id'       => $event->id],
            ['token'    => $auth['token']]
        );
        $this->assertTrue(in_array($this->response->status(), [200, 403]));
        // if ($this->response->status() == 200) {
        //     $this->notSeeInDatabase('event_user', [
        //         'event_id'  => $event->id,
        //         'user_id'   => $auth['user']->id,
        //     ]);
        // }
    }

    /**
     *  @test 
     *  @depends AuthTest::can_login
     *  @depends can_store_event
     */
    public function can_delete_event(array $auth, Event $event)
    {
        $response = $this->json(
            'POST',
            'v1/event/delete',
            ['id'       => $event->id],
            ['token'    => $auth['token']]
        );
        $this->assertEquals($this->response->status(), 200);
    }
}
