<?php

namespace App\Services\Validation\components\join;

use Carbon\Carbon;
use App\Models\Event;
use Illuminate\Http\Request;
use Morilog\Jalali\Jalalian;
use App\Services\Helper\Time;
use App\Services\Validation\contracts\Handler;

class ExpireHandler extends Handler
{
    public function process(Request $request,Event $event)
    {
        if ($event->start_at < time()){
            return [
                'message' => 'زمان شروع این رویداد گذشته است',
                'status'  => 'failed'
            ];
        }
        
        return parent::process($request, $event);
    }
}
