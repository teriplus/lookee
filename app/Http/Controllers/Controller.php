<?php

namespace App\Http\Controllers;

use App\Services\ACL\traits\Checkable;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use Checkable;
}
