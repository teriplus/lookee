<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->group(
    ['namespace'  => 'App'],
    function () use ($router) {
        $router->get('v1/email/verify', [
            'as'   => 'email.verify',
            'uses' => 'VerificationController@emailVerify'
        ]);
    }
);

$router->group(
    [
        // 'middleware' => 'jwt.auth',
        'namespace'  => 'App',
    ],
    function () use ($router) {
        $router->get('callback/{getway}', [
            'as'   => 'callback_getway',
            'uses' => 'Transaction\TransactionsController@callback'
        ]);
    }
);
