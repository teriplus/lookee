<?php

namespace App\Services\Helper;

use Illuminate\Support\Facades\Cache;

class Police
{
    public static function limit(string $key, int $maxTries, int $perSec, int $blockTimer = 0): bool
    {
        if ($blockTimer and Cache::get("block:$key")) {
            return false;
        }

        $limitable = Cache::get($key);

        if ($limitable) {
            if ($limitable >= $maxTries) {
                if ($blockTimer) Cache::set("block:$key", 1, $blockTimer);
                return false;
            }
            Cache::put($key, ++$limitable, $perSec);
        } else {
            Cache::set($key, 1, $perSec);
        }

        return true;
    }
}
