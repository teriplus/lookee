<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cache;

class AnalysisMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        
        $ttr = round(microtime(true) - LARAVEL_START, 5);
        $controllerName = $request->route()[1]['uses'] ?? $request->fullUrl();
        $controllerKey  = "analysis:" . $controllerName;

        $analysing = Cache::get($controllerKey);

        if ($analysing) {
            $count      = ++$analysing['count'];
            $last_ttr   = $analysing['ttr_avg'];
            $ttr_avg    = (($last_ttr * ($count - 1)) + $ttr) / $count;
            $data = [
                'count'     => $count,
                'ttr_avg'   => round($ttr_avg, 5)
            ];
            Cache::put($controllerKey, $data);
        } else {
            $data = [
                'count'     => 1,
                'ttr_avg'   => $ttr
            ];
            Cache::set($controllerKey, $data);
        }
        
        return $response;
    }
}