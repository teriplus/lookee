<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\ACL\Role;
use App\Services\ACL\ACL;
use App\Models\ACL\RoleUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolesController extends Controller
{
    public function __construct(Request $request)
    {
        $this->check(['roles', 'users'], $request->auth);
    }

    public function roles()
    {
        try {
            $roles = Role::orderBy('name')->with('permissions')->get();
            return response()->json([
                'data'   => $roles,
                'status' => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در بارگذاری مقام ها پیش آمده است',
                'status'  => 'failed',
            ], 500);
        }
    }
    
    public function admins()
    {
        try {
            $admins = RoleUser::pluck('user_id')->map(function ($id) {
                $user = User::whereId($id)
                    ->with('roles')
                    ->first()
                    ->only(array_merge(User::$showable, ['roles']));
                return $user;
            });
            return response()->json([
                'data'   => $admins,
                'status' => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در نمایش ادمین ها رخ داده است',
                'status'  => 'failed',
            ], 500);
        }
    }

    public function giveRoles(Request $request)
    {
        $this->validate($request, [
            'id'    => 'required|exists:users',
            'roles' => 'required|string'
        ]);

        try {
            $roles = explode(',', $request->input('roles'));
            ACL::giveRoles($roles, User::find($request->input('id')));

            return response()->json([
                'message' => 'مقام ها با موفقیت اضافه شدند',
                'status'  => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در اضافه کردن مقام ها پیش آمده است',
                'status'  => 'failed',
            ], 500);
        }
    }

    public function takeoverRoles(Request $request)
    {
        $this->validate($request, [
            'id'    => 'required|exists:users',
            'roles' => 'required|string'
        ]);

        try {
            $roles = explode(',', $request->input('roles'));
            ACL::takeOverRoles($roles, User::find($request->input('id')));

            return response()->json([
                'message' => 'مقام ها با موفقیت گرفته شدند',
                'status'  => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در گرفتن مقام ها پیش آمده است',
                'status'  => 'failed',
            ], 500);
        }
    }

    public function refreshRoles(Request $request)
    {
        $this->validate($request, [
            'id'    => 'required|exists:users',
            'roles' => 'required|string'
        ]);

        try {
            $roles = explode(',', $request->input('roles'));
            ACL::refreshRoles($roles, User::find($request->input('id')));

            return response()->json([
                'message' => 'مقام ها با موفقیت تغییر گرفته شدند',
                'status'  => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در تغییر مقام ها پیش آمده است',
                'status'  => 'failed',
            ], 500);
        }
    }
}
