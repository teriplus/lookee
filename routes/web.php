<?php

/** @var \Laravel\Lumen\Routing\Router $router */

$router->get('up', function () {
	return "Success";
});

/** @auth start */
$router->group(['namespace' => 'Auth'], function () use ($router) {
	$router->post('/login', 'AuthController@authenticate');
	$router->post('/register', 'AuthController@register');
	$router->post('/forget-password', 'AuthController@forget');
	$router->post('/reset-password', [
		'middleware' => 'jwt.auth',
		'uses' 		 => 'AuthController@reset'
	]);
});
/** @auth end */


/** @admin start */
$router->group(
	[
		'middleware' => ['jwt.auth', 'admin.auth'],
		'namespace'  => 'Admin',
		'prefix'     => 'admin'
	],
	function () use ($router) {
		/** @general */
		$router->post('/access', 'AdminController@access');

		/** @users */
		$router->post('/user', 'UsersController@user');
		$router->post('/user/delete', 'UsersController@deleteUser');
		$router->post('/user/active-vip', 'UsersController@activeVIP');
		$router->post('/user/inactive-vip', 'UsersController@inactiveVIP');
		$router->post('/user/activation/handler', 'UsersController@userActivationHandler');
		$router->post('/user/store-comment/handler', 'UsersController@commentHandler');
		$router->post('/user/join-event/handler', 'UsersController@joinEventHandler');
		$router->post('/user/create-event/handler', 'UsersController@createEventHandler');
		$router->post('/users/{page}', 'UsersController@users');

		/** @events */
		$router->post('/event', 'EventsController@event');
		$router->post('/event/status/handler', 'EventsController@statusHandler');
		$router->post('/events/{page}', 'EventsController@events');

		$router->post('/event/accept', 'EventsController@accept');
		$router->post('/event/unaccept', 'EventsController@unaccept');
		$router->post('/event/finish', 'EventsController@finish');
		$router->post('/event/delete', 'EventsController@delete');

		$router->post('/events/accept/many', 'EventsController@acceptMany');
		$router->post('/events/unaccept/many', 'EventsController@unacceptMany');
		$router->post('/events/finish/many', 'EventsController@finishMany');
		$router->post('/events/delete/many', 'EventsController@deleteMany');

		/** @payments */
		$router->post('/payment', 'PaymentsController@payment');
		$router->post('/payments', 'PaymentsController@payments');

		/** @support */
		$router->post('/XXX', 'SupportController@XXX');


		/** @statistics */
		$router->post('/statistics/users', 'StatisticsController@users');
		$router->post('/statistics/events', 'StatisticsController@events');
		$router->post('/statistics/payments', 'StatisticsController@payments');

		$router->post('/statistics/payments_in_days_ago', 'StatisticsController@paymentsInDaysAgo');
		$router->post('/statistics/events_in_days_ago', 'StatisticsController@eventsInDaysAgo');
		$router->post('/statistics/users_in_days_ago', 'StatisticsController@usersInDaysAgo');

		/** @roles */
		$router->post('/roles', 'RolesController@roles');
		$router->post('/admins', 'RolesController@admins');
		$router->post('/roles/give', 'RolesController@giveRoles');
		$router->post('/roles/takeover', 'RolesController@takeoverRoles');
		$router->post('/roles/refresh', 'RolesController@refreshRoles');

		/** @server */
		$router->post('/server/artisan', 'ServerController@artisan');
		$router->post('/server/redis', 'ServerController@redis');
	}
);
/** @admin end */


/** @general start */
$router->group(
	['namespace'  => 'App'],
	function () use ($router) {
		/** @events */
		$router->post('/event', 'EventsController@event');
		$router->post('/events/{page}', 'EventsController@events');
		$router->post('/event/comments', 'CommentsController@comments');
		$router->post('/event/members', 'EventsController@members');
	}
);
/** @general end */


/** @private start */
$router->group(
	[
		'middleware' => 'jwt.auth',
		'namespace'  => 'App',
	],
	function () use ($router) {
		/** @profile */
		$router->post('/profile', 'ProfileController@show');
		$router->post('/profile/edit', 'ProfileController@update');
		$router->post('/profile/phone-number/set', 'ProfileController@setPhoneNumber');
		$router->post('/profile/avatar/set', 'ProfileController@setAvatar');
		$router->post('/profile/avatar/delete', 'ProfileController@deleteAvatar');

		/** @events */
		$router->post('/my-events', 'EventsController@myEvents');
		$router->post('/event/store', 'EventsController@store');
		$router->post('/event/update', 'EventsController@update');
		$router->post('/event/delete', 'EventsController@delete');
		$router->post('/event/join', 'EventsController@join');
		$router->post('/event/leave', 'EventsController@leave');

		/** @actions */
		$router->post('/comment/store-event', 'CommentsController@store_event_comment');
		$router->post('/comment/store-reply', 'CommentsController@store_reply_comment');
		$router->post('/comment/update', 'CommentsController@update');
		$router->post('/comment/delete', 'CommentsController@delete');

		/** @VIP */
		$router->post('/vip/pay/1', 'VIPController@payOneMonth');
		$router->post('/vip/pay/3', 'VIPController@payThreeMoths');
		$router->post('/vip/info', 'VIPController@info');

		/** @helper */
		$router->post('/check/auth', 'UsersController@checkAuth');
		$router->post('/send/verification-email', 'VerificationController@sendVerificationEmail');
		// $router->post('/send/verification-sms', 'VerificationController@sendVerificationSms');
		
	}
);
/** @private end */

$router->post('tst', function () {
	return \App\Services\Helper\Time::timestampToDaykey(time());
});
