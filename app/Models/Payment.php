<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Payment extends Model
{
    use HasFactory;
    
    protected $table = "payments";

    public $timestamps = false;

    protected $fillable = [
        'user_id','unit', 'status', 'getway', 'refNum', 'created_at', 'verified_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
