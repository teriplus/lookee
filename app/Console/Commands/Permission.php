<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\ACL\Role as ACLRole;
use App\Models\ACL\RoleUser as ACLRoleUser;
use App\Models\ACL\Permission as ACLPermission;
use App\Models\ACL\PermissionRole as ACLPermissionRole;

class Permission extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'make:permission {--id=} {--to=}  {--truncate=}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'create permissions and roles --id=(user_id) --to=(super/admin) --truncate=(yes/no)';

	private static $permission_role = [
		['permission_id' => 1, 'role_id' => 1],
		['permission_id' => 2, 'role_id' => 1],
		['permission_id' => 3, 'role_id' => 1],
		['permission_id' => 4, 'role_id' => 1],
		['permission_id' => 5, 'role_id' => 1],
		['permission_id' => 6, 'role_id' => 1],
		['permission_id' => 7, 'role_id' => 1],

		['permission_id' => 1, 'role_id' => 2],
		['permission_id' => 2, 'role_id' => 2],
		['permission_id' => 4, 'role_id' => 2],
	];

	private static $permissions = [
		['name' => 'users', 		'label' => 'مدیریت کاربران']	, // 1
		['name' => 'events', 		'label' => 'مدیریت رویداد ها']	, // 2
		['name' => 'payments', 		'label' => 'مدیریت پرداخت ها']	, // 3
		['name' => 'support', 		'label' => 'پشتیبانی']			, // 4
		['name' => 'statistics', 	'label' => 'آمار ها']			, // 5
		['name' => 'roles',		 	'label' => 'مدیریت مقام ها']	, // 6
		['name' => 'server',		'label' => 'مدیریت سرور']		, // 7
	];

	private static $roles = [
		['name' => 'super', 'label' => 'مدیر کل']	, // 1
		['name' => 'admin', 'label' => 'مدیر']		, // 2
	];

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		if ( $this->option('truncate') == "yes" ) {
			ACLPermissionRole::truncate();
			ACLRoleUser::truncate();
			ACLPermission::truncate();
			ACLRole::truncate();
			foreach (self::$permissions as $permission) {
				ACLPermission::create($permission);
			}
	
			foreach (self::$roles as $role) {
				ACLRole::create($role);
			}
	
			foreach (self::$permission_role as $pr) {
				ACLPermissionRole::create($pr);
			}
		}

		if ($this->option('id') && $this->option('to')) {

			$role_id = ACLRole::where('name', $this->option('to'))->first()->id;

			ACLRoleUser::create([
				'role_id' => $role_id,
				'user_id' => $this->option('id')
			]);
		}

		echo "Access Control List Created Successfully" . PHP_EOL;
	}

	public static function getRoles()
	{
		$roles = [];
		foreach (self::$roles as $role) {
			array_push($roles, $role['name']);
		}

		return $roles;
	}
}
