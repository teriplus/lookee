<?php

namespace App\Services\Validation\components\join;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Services\Validation\contracts\Handler;

class StatusHandler extends Handler
{
    public function process(Request $request,Event $event)
    {
        if ($event->status == Event::$status['INIT']) {
            return [
                'message' => 'این رویداد فعلا تایید نشده است',
                'status'  => 'failed'
            ];
        }

        if ($event->status == Event::$status['DELETED']) {
            return [
                'message' => 'این رویداد حذف شده است',
                'status'  => 'failed'
            ];
        }

        if ($event->status == Event::$status['UNACCEPTED']) {
            return [
                'message' => 'این رویداد تایید نشد',
                'status'  => 'failed'
            ];
        }

        if ($event->status == Event::$status['FINISHED']) {
            return [
                'message' => 'این رویداد به پایان رسیده است',
                'status'  => 'failed'
            ];
        }

        return parent::process($request, $event);
    }
}
