<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\ACL\ACL;
use App\Console\Commands\Permission;

class AdminMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (ACL::hasAnyRoles(Permission::getRoles(), $request->auth)){
            return $next($request);
        }
        if (env("APP_DEBUG"))
            return abort(403);
        return abort(404);
    }
}
