<?php

namespace App\Http\Controllers\App\Transaction;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Services\Payment\Pay;
use App\Http\Controllers\Controller;
use App\Services\Payment\GetwayInterface;
use App\Http\Controllers\App\Transaction\Units\VIPUnitTrait;
use App\Http\Controllers\App\Transaction\Units\GeneralUnitTrait;

class TransactionsController extends Controller
{
    private Payment $payment;
    private Request $request;
    private $respnse;

    use VIPUnitTrait, GeneralUnitTrait;

    public function callback($getway, Request $request)
    {
        $this->payment = Payment::where("refNum", $request->trackId)->firstOrFail();
        $this->respnse = Pay::getway($getway)->verify($request);
        $this->request = $request;

        return
            $this->respnse['status'] == GetwayInterface::TRANSACTION_SUCCESS
            ? $this->transactionSuccess()
            : $this->transactionFailed();
    }

    public function getUser(): User
    {
        return User::findOrFail($this->payment->user_id);
    }

    private function transactionFailed()
    {
        $this->payment->update(array_merge($this->respnse, [
            "status" => GetwayInterface::TRANSACTION_FAIL,
        ]));

        return response()->json([
            'status'    => 'failed',
            'message'   => 'تراکنش ناموفق',
            'data'      => [
                "refNum"     => $this->payment->refNum,
                "getway"     => $this->payment->getway,
                "created_at" => $this->payment->created_at,
            ]
        ]);
    }

    private function transactionSuccess()
    {
        $method = $this->payment->unit;
        $exists = method_exists(new TransactionsController(), $method);
        if ($exists) {
            $this->$method($this->request);
        }

        $this->payment->update(array_merge($this->respnse, [
            "verified_at" => Carbon::now()->timestamp,
            "status" => GetwayInterface::TRANSACTION_SUCCESS,
        ]));

        return response()->json([
            'status'    => 'success',
            'message'   => 'تراکنش موفق',
            'data'      => [
                "refNum"     => $this->payment->refNum,
                "getway"     => $this->payment->getway,
                "created_at" => $this->payment->created_at,
            ]
        ]);
    }
}
