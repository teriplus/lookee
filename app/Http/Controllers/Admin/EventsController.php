<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EventsController extends Controller
{
    public function __construct(Request $request)
    {
        $this->check(['events'], $request->auth);
    }

    public function events($page, Request $request)
    {
        $this->validate($request, [
            'filter' => 'nullable|string'
        ]);

        $paginate = 30;

        $filter =  is_null($request->input('filter'))
            ? 'all'
            : htmlspecialchars($request->input('filter'));

        switch ($filter) {
            case 'all':
                try {
                    $events = Event
                        ::latest()
                        ->skip(($page - 1) * $paginate)
                        ->take($paginate)
                        ->get();

                    $totalEvents = Event::all()->count();
                    if ($totalEvents == 0) {
                        return response()->json([
                            'message' => 'این صفحه وجود ندارد',
                            'status'  => 'failed'
                        ], 404);
                    }

                    return response()->json([
                        'events'   => $events,
                        'paginate'   => (int) $paginate,
                        'page'       => (int) $page,
                        'pagesCount' => (int)  round($totalEvents / $paginate),
                        'status' => 'success',
                    ], 200);
                } catch (\Throwable $th) {
                    return response()->json([
                        'message' => 'مشکلی در بارگذاری رویدادها پیش آمده است',
                        'status'  => 'failed',
                    ], 500);
                }
            case $filter:
                $filter = strtoupper($filter);
                if (!isset(Event::$status[$filter])) {
                    return response()->json([
                        'message' => 'ورودی فیلتر نامعتبر است',
                        'valid' => [
                            'init', 'membering',
                            'deleted', 'finished',
                            'unaccepted',
                        ],
                        'status'  => 'failed',
                    ], 422);
                }
                try {
                    $events = Event
                        ::where('status', Event::$status[$filter])
                        ->skip(($page - 1) * $paginate)
                        ->take($paginate)
                        ->get();

                    $totalEvents = Event::all()->count();
                    if ($totalEvents == 0) {
                        return response()->json([
                            'message' => 'این صفحه وجود ندارد',
                            'status'  => 'failed'
                        ], 404);
                    }

                    return response()->json([
                        'data'   => $events,
                        'paginate'   => (int) $paginate,
                        'page'       => (int) $page,
                        'pagesCount' => (int)  round($totalEvents / $paginate),
                        'status' => 'success',
                    ], 200);
                } catch (\Throwable $th) {
                    return response()->json([
                        'message' => 'مشکلی در بارگذاری رویدادها پیش آمده است',
                        'status'  => 'failed',
                    ], 500);
                }
        }
    }

    public function event(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events'
        ]);

        try {
            $event = Event::find($request->input('id'));
            return response()->json([
                'data'   => $event,
                'status' => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در بارگذاری رویداد پیش آمده است',
                'status'  => 'failed',
            ], 500);
        }
    }

    public function accept(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events'
        ]);

        try {
            $event = Event::where('id', $request->input('id'))->first();

            if ($event->deleted_at != null) {
                return response()->json([
                    'message' => 'این رویداد قبلا حذف شده است و تایید برای این رویداد ممکن نیست',
                    'status'  => 'failed',
                ], 400);
            }

            $event->status = Event::$status['MEMBERING'];
            $event->save();
            return response()->json([
                'message' => 'رویداد تایید شد',
                'status'  => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در تایید رویداد پیش آمده است',
                'status'  => 'failed',
            ], 500);
        }
    }

    public function acceptMany(Request $request)
    {
        $this->validate($request, [
            'events_id' => 'required'
        ]);

        $events_id = explode(',', $request->input('events_id'));

        foreach ($events_id as $event_id) {
            $this->accept($request->merge(['id' => trim($event_id)]));
        }

        return response()->json([
            'message' => 'رویدادها تایید شدند',
            'status'  => 'success',
        ], 200);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events'
        ]);

        try {
            $event = Event::find($request->input('id'));
            $event->status     = Event::$status['DELETED'];
            $event->deleted_at = Carbon::now()->timestamp;
            $event->save();
            return response()->json([
                'message' => 'رویداد تایید شد',
                'status'  => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در حذف رویداد پیش آمده است',
                'status'  => 'failed',
            ], 500);
        }
    }

    public function deleteMany(Request $request)
    {
        $this->validate($request, [
            'events_id' => 'required'
        ]);

        $events_id = explode(',', $request->input('events_id'));

        foreach ($events_id as $event_id) {
            $this->delete($request->merge(['id' => trim($event_id)]));
        }

        return response()->json([
            'message' => 'رویدادها حذف شدند',
            'status'  => 'success',
        ], 200);
    }

    public function unaccept(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events'
        ]);

        try {
            $event = Event::find($request->input('id'));

            if ($event->deleted_at != null) {
                return response()->json([
                    'message' => 'این رویداد قبلا حذف شده است و رد کردن این رویداد ممکن نیست',
                    'status'  => 'failed',
                ], 400);
            }

            $event->status = Event::$status['UNACCEPTED'];
            $event->save();
            return response()->json([
                'message' => 'رویداد رد شد',
                'status'  => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در رد کردن رویداد پیش آمده است',
                'status'  => 'failed',
            ], 500);
        }
    }

    public function unacceptMany(Request $request)
    {
        $this->validate($request, [
            'events_id' => 'required'
        ]);

        $events_id = explode(',', $request->input('events_id'));

        foreach ($events_id as $event_id) {
            $this->unaccept($request->merge(['id' => trim($event_id)]));
        }

        return response()->json([
            'message' => 'رویدادها رد شدند',
            'status'  => 'success',
        ], 200);
    }

    public function finish(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events'
        ]);

        try {
            $event = Event::find($request->input('id'));

            if ($event->deleted_at != null) {
                return response()->json([
                    'message' => 'این رویداد قبلا حذف شده است و تمام کردن این رویداد ممکن نیست',
                    'status'  => 'failed',
                ], 400);
            }

            $event->status = Event::$status['FINISHED'];
            $event->save();
            return response()->json([
                'message' => 'رویداد به اتمام رسید',
                'status'  => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در به اتمام رساندن رویداد پیش آمده است',
                'status'  => 'failed',
            ], 500);
        }
    }

    public function finishMany(Request $request)
    {
        $this->validate($request, [
            'events_id' => 'required'
        ]);

        $events_id = explode(',', $request->input('events_id'));

        foreach ($events_id as $event_id) {
            $this->finish($request->merge(['id' => trim($event_id)]));
        }

        return response()->json([
            'message' => 'رویدادها به پایان رسیدند',
            'status'  => 'success',
        ], 200);
    }

    public function statusHandler(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            ['status' => 'required|integer',]
        );

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'failed',
                'message' => $validator->errors()->first()
            ], 422);
        }

        #redis
        if (env('CACHE_DRIVER') == 'redis') {
            $eventKeys = app('redis')->keys(env('CACHE_PREFIX') . ':events:*');
            foreach ($eventKeys as $key) {
                app('redis')->del($key);
            }
        }
        
        switch ($request->status) {
            case 1:
                return $this->accept($request);
            case 2:
                return $this->delete($request);
            case 3:
                return $this->finish($request);
            case 4:
                return $this->unaccept($request);
            default:
                return response()->json([
                    'status'  => 'failed',
                    'message' => 'ورودی وارد شده نامعتبر است'
                ], 422);
        }
    }
}
