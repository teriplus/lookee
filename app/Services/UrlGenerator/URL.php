<?php

namespace App\Services\UrlGenerator;

use Carbon\Carbon;
use Illuminate\Http\Request;

// HOTFIX
class URL
{
    public static $HASH_WRONG = "HASH_WRONG";
    public static $TIME_WRONG = "TIME_WRONG";
    public static $SUCCESS = "SUCCESS";

    public static function signature(string $routeName, array $params): string
    {
        $email      = $params['email'];
        $exp        = $params['exp'];
        $token      = sha1(implode('-*-', [$email, $exp]) . env('APP_KEY'));
        return $routeName . "/?email=$email&exp=$exp&token=$token";
    }

    public static function check(Request $request)
    {

        $email  = $request->input('email');
        $exp    = $request->input('exp');
        $token  = $request->input('token');
        $checkToken = sha1(implode('-*-', [$email, $exp]) . env('APP_KEY'));

        if ($token != $checkToken) {
            return self::$HASH_WRONG;
        }

        if ($exp < time()) {
            return self::$TIME_WRONG;
        }

        return self::$SUCCESS;
    }
}
