<?php

use App\Models\Event;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Events extends Migration
{

    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('string_id', 14)->unique();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('title');
            $table->string('slug')->unique();
            $table->tinyText('description')->nullable();
            $table->text('content');
            $table->tinyText('images')->nullable();

            $table->string('origin_city', 50)->nullable()->default("-");
            $table->string('origin_state', 50)->nullable()->default("-");
            $table->string('destination_city', 50);
            $table->string('destination_state', 50)->nullable()->default("-");

            $table->string('price', 10)->default(0)->comment('Rials');
            $table->string('recommended_price', 10)->default(0)->comment('Rials');

            $table->tinyInteger('min_member')->nullable();
            $table->tinyInteger('max_member')->nullable();
            $table->tinyInteger('members')->default(1);
            $table->tinyInteger('min_age')->nullable();
            $table->tinyInteger('max_age')->nullable();
            $table->tinyInteger('gender')->comment('1:male, 2:female');
            $table->tinyInteger('status')->default(Event::$status['MEMBERING']);
            $table->tinyInteger('level')->nullable();

            $table->string('start_at', 16);
            $table->string('end_at', 16);
            $table->string('created_at', 16)->nullable();
            $table->string('updated_at', 16)->nullable();
            $table->string('deleted_at', 16)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('events');
    }
}
