<?php

namespace App\Http\Middleware;

use Closure;
use App\Services\Helper\Police;
use Illuminate\Support\Facades\Cache;

class GuardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $guardKey   = "guard:" . $request->getClientIp();
        $maxTries   = 300;
        $perSec     = 600;
        $blockTimer = 60 * 30;

        if (Police::limit($guardKey, $maxTries, $perSec, $blockTimer)) {
            return $next($request);
        }

        $perSec = $perSec / 60;
        $blockTimer = $blockTimer / 60;
        return response()->json([
            "status" => "failed",
            "message" => "سرور در $perSec دقیقه گذشته $maxTries درخواست شما را پاسخ داده است لطفا $blockTimer دقیقه دیگه تلاش کنید"
        ], 429);
    }
}
