<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Comment extends Model
{
    use HasFactory;

    protected $table = "comments";

    public $timestamps = false;

    protected $guarded = [];

    public static $updatable = [
        'body'
    ];

    public static $status = [
        'INACTIVE' => 0,
        'ACTIVE'   => 1,
    ];

    public function commentable()
    {
        return $this->morphTo();
    }
    
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
