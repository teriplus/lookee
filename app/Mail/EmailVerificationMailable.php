<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Services\UrlGenerator\URL;
use Illuminate\Queue\SerializesModels;

class EmailVerificationMailable extends Mailable
{
    use Queueable, SerializesModels;

    public User $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('mail.EmailVerification')->with([
            'name' => $this->user->name,
            'link' => $this->urlGenerator()
        ]);
    }

    private function urlGenerator()
    {
        return URL::signature(
            env('APP_URL') . "/api/v1/email/verify",
            [
                'email' => $this->user->email,
                'exp'   => strtotime("+2 hours"),
            ]
        );
    }
}
