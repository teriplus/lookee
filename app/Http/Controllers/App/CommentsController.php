<?php

namespace App\Http\Controllers\App;

use App\Models\Event;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CommentsController extends Controller
{
    private $comment;

    public function __construct(Request $request)
    {
        $this->comment = new Comment();
        $this->comment->user_id = $request->auth->id ?? null;
        $this->comment->created_at = Carbon::now()->timestamp;
    }

    public function store_event_comment(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events',
            'body' => 'required'
        ]);

        $body = trim($request->input('body'));
        if ($body == "") {
            return response()->json([
                'message' => 'پیام نمی تواند خالی باشد',
                'status'  => 'failed',
            ], 403);
        }
        $body = htmlspecialchars($body);

        try {
            $event = Event::whereId($request->input('id'))->firstOrFail();
            $this->comment->body = $body;
            $event->comments()->save($this->comment);

            return response()->json([
                'message' => 'پیام باموفقیت ثبت شد',
                'status' => 'success'
            ], 201);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'در ثبت پیام مشکلی رخ داده است',
                'status' => 'failed'
            ], 500);
        }
    }

    public function store_reply_comment(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:comments',
            'body' => 'required'
        ]);

        $body = trim($request->input('body'));
        if ($body == "") {
            return response()->json([
                'message' => 'پیام نمی تواند خالی باشد',
                'status'  => 'failed',
            ], 403);
        }
        $body = htmlspecialchars($body);

        try {
            $comment = Comment::whereId($request->input('id'))->firstOrFail();
            $this->comment->body = $body;
            $comment->comments()->save($this->comment);

            return response()->json([
                'message' => 'پیام باموفقیت ثبت شد',
                'status' => 'success'
            ], 201);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'در ثبت پیام مشکلی رخ داده است',
                'status' => 'failed'
            ], 500);
        }
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'id'   => 'required|exists:comments',
            'body' => 'required'
        ]);

        try {
            $comment = Comment::whereId($request->input('id'))
                ->where('deleted_at', null)
                ->firstOrFail();

            if ($request->auth->id != $comment->user_id) {
                return response()->json([
                    'message' => 'شما مجار به ویرایش این پیام نیستید',
                    'status'  => 'failed'
                ], 403);
            }

            $comment->body = $request->input('body');
            $comment->updated_at = Carbon::now()->timestamp;
            $comment->save();

            return response()->json([
                'message' => 'پیام باموفقیت ویرایش شد',
                'status' => 'success'
            ], 201);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'در ویرایش پیام مشکلی رخ داده است',
                'status' => 'failed'
            ], 500);
        }
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:comments',
        ]);

        try {
            $comment = Comment::whereId($request->input('id'))
                ->where('deleted_at', null)
                ->firstOrFail();

            if ($request->auth->id != $comment->user_id) {
                return response()->json([
                    'message' => 'شما مجار به حذف این پیام نیستید',
                    'status'  => 'failed'
                ], 403);
            }

            $comment->deleted_at = Carbon::now()->timestamp;
            $comment->save();

            return response()->json([
                'message' => 'پیام باموفقیت حذف شد',
                'status' => 'success'
            ], 201);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'در حذف پیام مشکلی رخ داده است',
                'status' => 'failed'
            ], 500);
        }
    }

    public function comments(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events',
        ]);

        try {
            $event = Event::whereId($request->input('id'))->firstOrFail();

            $event = collect($event->comments)->map(function ($comment) {
                $this->repliesFollowUp($comment);
                return $comment;
            });

            return response()->json([
                'data'   => $event,
                'status' => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در بارگذاری نظرات پیش آمده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    private function repliesFollowUp($target): void
    {
        $target->owner = [
            'id'     => $target->user->id,
            'name'   => $target->user->name,
            'avatar' => ProfileController::getAvatar($target->user->avatar),
        ];
        unset($target->user);
        foreach ($target->comments as $reply) {
            if ($reply->count() != 0) {
                $this->repliesFollowUp($reply);
            }
        }
    }
}
