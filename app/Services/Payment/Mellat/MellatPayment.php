<?php

namespace App\Services\Payment\Mellat;

use Illuminate\Http\Request;
use App\Services\Payment\GetwayInterface;

class MellatPayment implements GetwayInterface
{
    protected static $name;
    protected static $instance;
    protected static $merchantID;
    protected static $callbackRoute;

    protected function __construct()
    {
        self::$name = "mellat";
        self::$merchantID    = env('MERCHANT_ID_MELLAT', null);
        self::$callbackRoute = route('callback_getway', [
            "getway" => self::$name
        ]);
    }

    public static function getSupportTransaction()
    {
        if (is_null(self::$instance)) {
            return new static();
        }
        return self::$instance;
    }


    public function pay(array $options): void
    {
        # code
    }

    public function verify(Request $request): array
    {
        return [];
    }
}
