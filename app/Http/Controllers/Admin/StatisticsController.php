<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use App\Services\Payment\GetwayInterface;

class StatisticsController extends Controller
{
    public function __construct(Request $request)
    {
        $this->check(['statistics'], $request->auth);
    }

    public function users()
    {
        try {
            $userCount = User::count();
            return response()->json([
                'status' => 'success',
                'data'   => [
                    'count' => $userCount
                ]
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status'  => 'failed',
                'message' => 'مشکلی در نمایش تعداد کاربران پیش آمده است'
            ], 500);
        }
    }

    public function usersInDaysAgo(Request $request)
    {
        /**
         * 1 => today
         * 2 => yesterday
         * 3 => ...
         */

        $this->validate($request, [
            'day_ago' => 'required|string'
        ]);

        $dayAgo = $request->day_ago;
        $dayAgoTomorrow = $request->day_ago - 1;
        try {
            $userCount = User
                ::where('created_at', '>', strtotime("-$dayAgo day"))
                ->where('created_at', '<', strtotime("-$dayAgoTomorrow day"))
                ->count();
            return response()->json([
                'status' => 'success',
                'data'   => [
                    'count' => $userCount
                ]
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status'  => 'failed',
                'message' => 'مشکلی در نمایش تعداد کاربران پیش آمده است'
            ], 500);
        }
    }

    public function events(Request $request)
    {
        $this->validate($request, [
            'filter' => 'nullable|string'
        ]);

        $filter =  is_null($request->input('filter'))
            ? 'all'
            : htmlspecialchars($request->input('filter'));

        switch ($filter) {
            case 'all':
                try {
                    $eventCount = Event::count();
                    return response()->json([
                        'status' => 'success',
                        'data'   => [
                            'count' => $eventCount
                        ]
                    ], 200);
                } catch (\Throwable $th) {
                    return response()->json([
                        'status'  => 'failed',
                        'message' => 'مشکلی در بارگذاری تعداد رویدادها پیش آمده است',
                    ], 500);
                }
            case $filter:
                $filter = strtoupper($filter);
                if (!isset(Event::$status[$filter])) {
                    return response()->json([
                        'message' => 'ورودی فیلتر نامعتبر است',
                        'data' => [
                            'valid' => [
                                'init', 'membering',
                                'deleted', 'finished',
                                'unaccepted',
                            ]
                        ],
                        'status'  => 'failed',
                    ], 422);
                }
                try {
                    $eventCount = Event::where('status', Event::$status[$filter])
                        ->count();
                    return response()->json([
                        'status' => 'success',
                        'data'   => [
                            'count' => $eventCount
                        ]
                    ], 200);
                } catch (\Throwable $th) {
                    return response()->json([
                        'message' => 'مشکلی در بارگذاری تعداد رویدادها پیش آمده است',
                        'status'  => 'failed',
                    ], 500);
                }
        }
    }

    public function eventsInDaysAgo(Request $request)
    {
        /**
         * 1 => today
         * 2 => yesterday
         * 3 => ...
         */

        $this->validate($request, [
            'day_ago' => 'required|string'
        ]);

        $dayAgo = $request->day_ago;
        $dayAgoTomorrow = $request->day_ago - 1;
        try {
            $eventCount = Event
                ::where('created_at', '>', strtotime("-$dayAgo day"))
                ->where('created_at', '<', strtotime("-$dayAgoTomorrow day"))
                ->count();
            return response()->json([
                'status' => 'success',
                'data'   => [
                    'count' => $eventCount
                ]
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status'  => 'failed',
                'message' => 'مشکلی در نمایش تعداد رویدادها پیش آمده است'
            ], 500);
        }
    }

    public function payments(Request $request)
    {
        $this->validate($request, [
            'filter' => 'nullable|string'
        ]);

        if (is_null($request->input('filter'))) {
            $payments = Payment::count();
            return response()->json([
                'status'  => 'success',
                'data'    => $payments
            ], 200);
        } else {
            $payments = Payment::where('status', $request->filter)->count();
            return response()->json([
                'status'  => 'success',
                'data'    => $payments
            ], 200);
        }
    }

    public function paymentsInDaysAgo(Request $request)
    {
        /**
         * 1 => today
         * 2 => yesterday
         * 3 => ...
         */

        $this->validate($request, [
            'day_ago' => 'required|string'
        ]);

        $dayAgo = $request->day_ago;
        $dayAgoTomorrow = $request->day_ago - 1;
        try {
            $paymentCount = Payment
                ::where('created_at', '>', strtotime("-$dayAgo day"))
                ->where('created_at', '<', strtotime("-$dayAgoTomorrow day"))
                ->count();
            return response()->json([
                'status' => 'success',
                'data'   => [
                    'count' => $paymentCount
                ]
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status'  => 'failed',
                'message' => 'مشکلی در نمایش تعداد پرداخت ها پیش آمده است'
            ], 500);
        }
    }
}
