<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Event;
use Illuminate\Database\Eloquent\Factories\Factory;

class EventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Event::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "title"             => $this->faker->sentence(),
            "description"       => $this->faker->sentence(),
            "content"           => $this->faker->paragraph(),
            "string_id"         => 'f_' . bin2hex(openssl_random_pseudo_bytes(6)),
            "user_id"           => User::factory()->create()->id,
            "images"            => null,
            "origin_city"       => $this->faker->city,
            "origin_state"      => $this->faker->state,
            "destination_city"  => $this->faker->city,
            "destination_state" => $this->faker->state,
            "price"             => [0, 100000, 100000][rand(0, 2)],
            "recommended_price" => [0, 100000, 100000][rand(0, 2)],
            "min_member"        => rand(2, 5),
            "max_member"        => rand(5, 20),
            "min_age"           => rand(18, 20),
            "max_age"           => rand(20, 30),
            "gender"            => rand(0, 3),
            // "start_at"          => strtotime("+2 days"),
            // "end_at"            => strtotime("+3 days"),
        ];
    }

    public function unDirectly()
    {
        return $this->state(function (array $attributes) {
            return [
                "start_day"         => "14101110",
                "end_day"           => "14101111",
                "start_time"        => "1300",
                "end_time"          => "1800",
            ];
        });
    }

    public function directly()
    {
        return $this->state(function (array $attributes) {
            return [
                "start_at"          => strtotime("+2 days"),
                "end_at"            => strtotime("+3 days"),
            ];
        });
    }
}
