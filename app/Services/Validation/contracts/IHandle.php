<?php

namespace App\Services\Validation\contracts;

use App\Models\Event;
use Illuminate\Http\Request;

interface IHandle 
{
    public function next($nextHandler);
    public function process(Request $request,Event $event);
}