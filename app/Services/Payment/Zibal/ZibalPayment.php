<?php

namespace App\Services\Payment\Zibal;

use Carbon\Carbon;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Services\Payment\GetwayInterface;

class ZibalPayment implements GetwayInterface
{
    protected static $name;
    protected static $instance;
    protected static $merchantID;
    protected static $callbackRoute;

    protected function __construct()
    {
        self::$name = "zibal";
        self::$merchantID    = env('MERCHANT_ID_ZIBAL', null);
        self::$callbackRoute = route('callback_getway', [
            "getway" => self::$name
        ]);
    }

    public static function getSupportTransaction()
    {
        if (is_null(self::$instance)) {
            return new static();
        }
        return self::$instance;
    }

    public function pay(array $options): void
    {
        $response = $this->postToZibal('request', $options);
        $this->insertToDatabase($response, $options);
        $this->redirectToBank($response);
    }

    public function verify(Request $request): array
    {
        if (!$request->has('success') || $request->input('success') != 1) {
            return [
                'status' => GetwayInterface::TRANSACTION_FAIL,
                'getway' => self::$name,
                'refNum' => $request->input('trackId')
            ];
        }

        return $this->verifyToZibal($request) ?
            [
                'status' => GetwayInterface::TRANSACTION_SUCCESS,
                'getway' => self::$name,
                'refNum' => $request->input('trackId')
            ] :
            [
                'status' => GetwayInterface::TRANSACTION_FAIL,
                'getway' => self::$name,
                'refNum' => $request->input('trackId')
            ];
    }


    /**
     * Private Functions
     */

    private function postToZibal($path, $options)
    {
        $url = 'https://gateway.zibal.ir/v1/' . $path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->getParams($options)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response  = curl_exec($ch);
        curl_close($ch);
        return json_decode($response);
    }

    private function verifyToZibal($request)
    {
        $parameters = array(
            "merchant"   => self::$merchantID,
            "trackId"    => $request->input('trackId'),
        );

        $response = $this->postToZibal('verify', $parameters);

        if ($response->result == 100) return true;

        return false;
    }


    private function redirectToBank($response)
    {
        if ($response->result == 100) {
            $startGateWayUrl = "https://gateway.zibal.ir/start/" . $response->trackId;
            header("Location: $startGateWayUrl");
            exit();
        }
        Log::emergency(" class: " . ZibalPayment::class . " RedirectToBank Problem " . " $response->message");
    }

    private function insertToDatabase($response, $options)
    {
        Payment::create([
            'user_id' => $options['user']->id,
            'status'  => GetwayInterface::TRANSACTION_ONPAY,
            'getway'  => self::$name,
            'unit'    => $options['unit'],
            'refNum'  => $response->trackId,
            "created_at" => Carbon::now()->timestamp,
        ]);
    }

    private function getParams(array $options)
    {
        return array_merge(array_merge(
            $options,
            [
                "merchant"    => self::$merchantID,
                "callbackUrl" => self::$callbackRoute,
            ]
        ));
    }
}
