<?php

namespace App\Services\Validation\components\join;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Services\Validation\contracts\Handler;

class UserHandler extends Handler
{
    public function process(Request $request, Event $event)
    {
        if ($request->auth->access_join_event != 1) {
            return [
                'message' => 'شما مجاز به پیوستن به هیچ رویدادی نیستید',
                'status'  => 'failed'
            ];
        }

        return parent::process($request, $event);
    }
}
