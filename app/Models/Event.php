<?php

namespace App\Models;

use App\Models\User;
use App\Models\Comment;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Pishran\LaravelPersianSlug\HasPersianSlug;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Event extends Model
{
    use HasPersianSlug, HasFactory;

    protected $table = "events";

    public $timestamps = false;

    protected $guarded = [];

    public static $updatable = [
        'title', 'description', 'content',
        'origin_city', 'origin_state', 'destination_city',
        'destination_state', 'price', 'recommended_price',
        'min_member', 'max_member', 'min_age', 'max_age',
        'gender',
    ];

    public static $status = [
        'INIT'       => 0,
        'MEMBERING'  => 1,
        'DELETED'    => 2,
        'FINISHED'   => 3,
        'UNACCEPTED' => 4,
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public static function generateStringId()
    {
        $limit = 0;
        do {
            $string_id = 'u_' . bin2hex(openssl_random_pseudo_bytes(6));
            $exists    = Event::where('string_id', $string_id)->exists();
            if ($limit++ > 100) {
                return response()->json([
                    'message' => 'در ساخت رویداد جدید مشکلی رخ داده است. لطفا مجددا تلاش کنید',
                    'status'  => 'failed'
                ], 500);
            }
        } while ($exists);
        return $string_id;
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')
            ->where('status', Comment::$status['ACTIVE'])
            ->orderBy('created_at', 'DESC');
    }

    public function members()
    {
        return $this->belongsToMany(User::class);
    }
}
