<?php

namespace App\Listeners;

use App\Events\UserRegisterEvent;
use App\Jobs\SendVerificationEmail;

class SendVerificationEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ExampleEvent  $event
     * @return void
     */
    public function handle(UserRegisterEvent $event)
    {
        dispatch(new SendVerificationEmail($event->user));
    }
}
