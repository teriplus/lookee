<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration{
    
    public function up(){
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('avatar')->nullable();
            $table->string('user_name')->unique()->nullable();
            $table->string('birth_day', 8)->nullable();
            $table->tinyInteger('gender')->default(3);

            $table->string('email')->unique();
            $table->string('password');
            $table->string('phone_number')->unique()->nullable();

            $table->text('bio')->nullable();
            $table->integer('score')->default(0);
            $table->string('bank_account')->nullable()->comment('Bank Accont Number 16 digit');

            $table->integer('wallet')->default(0);
            $table->integer('wallet_hash')->nullable();

            $table->tinyInteger('status')->default(User::$status['PENDING']);
            $table->tinyInteger('vip_status')->default(User::$vip_status['INACTIVE']);
            $table->string('vip_expire_at')->nullable();

            $table->tinyInteger('access_comment')->default(1);
            $table->tinyInteger('access_join_event')->default(1);
            $table->tinyInteger('access_create_event')->default(1);

            $table->string('instagram_id')->nullable();
            $table->string('telegram_id')->nullable();

            $table->string('created_at', 16)->nullable();
            $table->string('updated_at', 16)->nullable();
            $table->string('email_verified_at', 16)->nullable();
            $table->string('phone_number_verified_at', 16)->nullable();
            $table->string('deleted_at', 16)->nullable();
        });
    }

    public function down(){
        Schema::dropIfExists('users');
    }
}
