<?php

namespace App\Http\Controllers\App;

use Carbon\Carbon;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\UrlGenerator\URL;
use App\Jobs\SendVerificationEmail;
use App\Http\Controllers\Controller;
use App\Services\Helper\Police;

class VerificationController extends Controller
{
    public function emailVerify(Request $request)
    {
        switch (URL::check($request)) {
            case URL::$SUCCESS;
                return $this->successResponce($request);
            case URL::$HASH_WRONG;
                return $this->hashWrongResponce();
            case URL::$TIME_WRONG;
                return $this->timeWrongResponce();
        }
    }

    public function sendVerificationEmail(Request $request)
    {
        $redisKey        = "send_verification_email_" . $request->auth->email;
        $redisExpireTime = env('DEBUG') ? 1 : 5 * 60;
        $maxTries        = env('DEBUG') ? 10 : 2;

        try {
            if (Police::limit($redisKey, $maxTries, $redisExpireTime)) {
                dispatch_now(new SendVerificationEmail($request->auth));
                return response()->json([
                    'status' => 'success',
                    'message' => 'تاییدیه ایمیل ارسال شد'
                ], 200);
            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'لطفا ۵ دقیقه ی دیگر مجددا تلاش نمایید'
                ], 403);
            }
        } catch (\Throwable $th) {
            // throw new \Exception($th);
            return response()->json([
                'status' => 'falied',
                'message' => 'مشکلی در ارسال تاییدیه ایمیل به وجود آمده است'
            ], 500);
        }
    }

    private function successResponce(Request $request)
    {
        try {
            $user = User::where('email', $request->input('email'))->firstOrFail();
            if ($user->status == User::$status['PENDING']) {
                $user->status = User::$status['ACTIVE'];
                $user->email_verified_at = Carbon::now()->timestamp;
                $user->save();

                return 'You Are Active Now';
            }
            return 'You Are not in pending state';
        } catch (\Throwable $th) {
            // throw new \Exception($th);
            return 'Problem in Activation';
        }
    }

    private function hashWrongResponce()
    {
        return 'Problem in Activation, hashWrong';
    }

    private function timeWrongResponce()
    {
        return 'Problem in Activation, timeWrong';
    }
}
