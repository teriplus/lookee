<?php

namespace App\Http\Controllers\App;

use App\Models\Event;
use App\Models\EventUser;

use Illuminate\Http\Request;
use App\Services\Helper\Time;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use App\Services\Validation\components\join\UserHandler;

use App\Services\Validation\components\join\LimitHandler;
use App\Services\Validation\components\join\ExpireHandler;
use App\Services\Validation\components\join\StatusHandler;


class EventsController extends Controller
{

    /**
     * event validation roles
     *
     * @var array
     */
    private static $eventValidation = [
        'title'        => 'required|min:5',
        'description'  => 'required|min:10|max:500',
        'content'      => 'required|min:10',
        // 'image'       => 'mimes:jpeg,png,jpg|max:1024|nullable',

        'start_day'  => 'required|numeric|regex:/^\d{8}$/u',
        'start_time' => 'required|numeric|regex:/^\d{4}$/u',
        'end_day'    => 'required|numeric|regex:/^\d{8}$/u',
        'end_time'   => 'required|numeric|regex:/^\d{4}$/u',

        'origin_city'  => 'nullable|string|max:200',
        'origin_state' => 'nullable|string|max:200',
        'destination_city'  => 'required|string|max:200',
        'destination_state' => 'nullable|string|max:200',
        'price'             => 'nullable',
        'recommended_price' => 'nullable',
        'min_age'    => 'numeric',
        'max_age'    => 'numeric',
        'min_member'        => 'required|numeric',
        'max_member'        => 'required|numeric',
        'gender'     => 'required|numeric',
    ];

    /**
     * return a event
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function event(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id' => 'required|exists:events'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'failed',
                'message' => $validator->errors()->first()
            ], 422);
        }

        try {
            $event = Event::whereId($request->input('id'))
                ->where('status', Event::$status['MEMBERING'])
                ->orWhere('status', Event::$status['FINISHED'])
                ->with('owner:id,name,avatar')
                ->firstOrFail();

            $event->image = $event->image != null ?
                env('APP_URL') . '/upload/events/' . $event->image :
                env('APP_URL') . "/static/null_event_image.png";

            $event->start_day       = Time::timestampToDaykey($event->start_at, true);
            $event->start_time      = Time::timestampToTimekey($event->start_at, true);
            $event->end_day         = Time::timestampToDaykey($event->end_at, true);
            $event->end_time        = Time::timestampToTimekey($event->end_at, true);
            $event->genderNumeric   = $event->gender;

            switch ($event->gender) {
                case 1:
                    $event->gender = "فقط آقایان";
                    break;
                case 2:
                    $event->gender = "فقط بانوان";
                    break;
                default:
                    $event->gender = "اهمیتی ندارد";
            }



            return response()->json([
                'event'     => $event,
                'status'    => 'success',
            ], 200);
        } catch (\Throwable $th) {
            // throw new \Exception($th);
            return response()->json([
                'message' => 'شناسه انتخاب شده، معتبر نیست.',
                'status'  => 'failed',
            ], 500);
        }
    }

    /**
     * return evetns list
     *
     * @param Request $request
     * @param integer $page
     * @return JsonResponse
     */
    public function events($page, Request $request)
    {
        $paginate   = 20;
        $filter     = 0; //TODO

        $eventsKey      = "events:" . $page . "_" . $filter;
        $totalEventsKey = "events:totalCount";

        $eventsExpireAt      = 60 * 10;
        $totalEventsExpireAt = 60 * 10;

        $events = Cache::remember($eventsKey, $eventsExpireAt, function () use ($page, $paginate) {
            try {
                $events = Event
                    ::where('status', Event::$status['MEMBERING'])
                    ->orWhere('status', Event::$status['FINISHED'])
                    ->orderBy('status', 'DESC')
                    ->latest()
                    ->skip(($page - 1) * $paginate)
                    ->take($paginate)
                    ->get()
                    ->map(function ($event) {
                        return self::showableEvent($event);
                    });

                return $events;
            } catch (\Throwable $th) {
                // throw new \Exception($th);
                return response()->json([
                    'message' => 'مشکلی در نمایش رویدادها رخ داده است',
                    'status'  => 'failed'
                ], 500);
            }
        });

        $totalEvents = Cache::remember($totalEventsKey, $totalEventsExpireAt, function () {
            return Event
                ::where('status', Event::$status['MEMBERING'])
                ->orWhere('status', Event::$status['FINISHED'])
                ->count();
        });

        if ($events->count() == 0 && $page != 1) {
            return response()->json([
                'message' => 'این صفحه وجود ندارد',
                'status'  => 'failed'
            ], 404);
        }

        return response()->json([
            'events'     => $events,
            'total'      => (int) $totalEvents,
            'paginate'   => (int) $paginate,
            'page'       => (int) $page,
            'pagesCount' => (int)  round($totalEvents / $paginate),
            'status'     => 'success'
        ], 200);
    }

    /**
     * return user events
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function myEvents(Request $request)
    {
        $events = $request->auth->events
            ->filter(function ($event) {
                return $event->status == Event::$status['INIT'] || $event->status == Event::$status['MEMBERING'];
            })
            ->map(function ($event) {
                return self::showableEvent($event);
            });
        if (!$events->count()) {
            return response()->json([
                'message' => 'شما هیچ رویدادی ثبت نکرده اید',
                'status' => 'failed'
            ], 400);
        }
        return response()->json([
            'events' => $events,
            'status' => 'success'
        ], 200);
    }

    /**
     * store new event
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if ($request->auth->access_create_event != 1) {
            return response()->json([
                'message' => 'شما مجاز به ایجاد رویداد جدید نیستید',
                'status'  => 'failed'
            ], 403);
        }

        $validator = Validator::make($request->all(), self::$eventValidation);

        if ($validator->fails()) {
            return response()->json([
                'status'  => 'failed',
                'message' => $validator->errors()->first()
            ], 422);
        }

        $start_at = Time::daykeyToTimestamp($request->start_day, $request->start_time);
        $end_at = Time::daykeyToTimestamp($request->end_day, $request->end_time);

        if ($start_at < time()) {
            return response()->json([
                'status'  => 'failed',
                'message' => "تاریخ شروع نمی تواند گذشته باشد"
            ], 422);
        }

        if ($start_at > $end_at) {
            return response()->json([
                'status'  => 'failed',
                'message' => "تاریخ شروع نمی تواند بزرگ تر از تاریخ پایان باشد"
            ], 422);
        }

        if ($request->max_member < $request->min_member) {
            return response()->json([
                'status'  => 'failed',
                'message' => "حداکثر اعضا باید بیشتر از حداقل اعضا باشد"
            ], 422);
        }

        try {
            $event = $request->auth->events()->create(
                array_merge(
                    $request->only(Event::$updatable),
                    [
                        'string_id'     => Event::generateStringId(),
                        'start_at'      => $start_at,
                        'end_at'        => $end_at,
                        "created_at"    => time(),
                    ]
                )
            );

            EventUser::create([
                'user_id'    => $request->auth->id,
                'event_id'   => $event->id,
                'created_at' => time()
            ]);

            // $image        = $request->file('image');
            // $path         = $image->store('upload/events');
            // $name         = explode(DIRECTORY_SEPARATOR, $path)[2];
            // $event->image = $name;
            // $event->save();

            return response()->json([
                'message' => 'رویداد با موفقیت ثبت شد و تا دقایق آینده منتشر خواهد شد',
                'status'  => 'success'
            ], 201);
        } catch (\Throwable $th) {
            throw new \Exception($th);
            return response()->json([
                'message' => 'مشکلی در ثبت رویداد رخ داده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    /**
     * update an event
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events',
            'data' => 'nullable'
        ]);

        try {
            $event = Event::where('id', $request->only('id'))->firstOrFail();

            if ($event->user_id != $request->auth->id) {
                return response()->json([
                    'message' => 'شما مجار به ویرایش این رویداد نیستید',
                    'status'  => 'failed'
                ], 403);
            }

            if ($request->auth->access_create_event != 1) {
                return response()->json([
                    'message' => 'شما مجاز به ایجاد/ویرایش رویداد جدید نیستید',
                    'status'  => 'failed'
                ], 403);
            }

            // return event data for update
            if ($request->data == 'true') {
                return response()->json([
                    'status' => 'success',
                    'event'  => array_merge([
                        'start_day'     => Time::timestampToDaykey($event->start_at),
                        'start_time'    => Time::timestampToTimekey($event->start_at),
                        'end_day'       => Time::timestampToDaykey($event->end_at),
                        'end_time'      => Time::timestampToTimekey($event->end_at),
                    ], $event->only(Event::$updatable)),
                ], 200);
            }

            $validator = Validator::make($request->all(), self::$eventValidation);

            if ($validator->fails()) {
                return response()->json([
                    'status'  => 'failed',
                    'message' => $validator->errors()->first()
                ], 422);
            }

            if (Time::daykeyToTimestamp($request->start_day, $request->start_time) < time()) {
                return response()->json([
                    'status'  => 'failed',
                    'message' => "تاریخ شروع نمی تواند گذشته باشد"
                ], 422);
            }

            if (Time::daykeyToTimestamp($request->start_day, $request->start_time) > Time::daykeyToTimestamp($request->end_day, $request->end_time)) {
                return response()->json([
                    'status'  => 'failed',
                    'message' => "تاریخ شروع نمی تواند بزرگ تر از تاریخ پایان باشد"
                ], 422);
            }

            if ($request->max_member < $request->min_member) {
                return response()->json([
                    'status'  => 'failed',
                    'message' => "حداکثر اعضا باید بیشتر از حداقل اعضا باشد"
                ], 422);
            }

            $event->update(array_merge(
                $request->only(Event::$updatable),
                [
                    // 'status' => Event::$status['INIT'],
                    'status' => Event::$status['MEMBERING'], // HOTFIX
                    'updated_at' => time(),
                ]
            ));

            return response()->json([
                'message' => 'رویداد باموفقیت به روزرسانی شد',
                'status'  => 'success'
            ], 200);
        } catch (\Throwable $th) {
            throw new \Exception($th);
            return response()->json([
                'message' => 'در بروزرسانی رویداد مشکلی رخ داده است. لطفا مجددا تلاش کنید',
                'status'  => 'failed'
            ], 500);
        }
    }

    /**
     * delete an event
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events'
        ]);

        try {
            $event = Event::where('id', $request->only('id'))->firstOrFail();

            if ($event->user_id != $request->auth->id) {
                return response()->json([
                    'message' => 'شما مجار به حذف این رویداد نیستید',
                    'status'  => 'failed'
                ], 403);
            }

            $event->update([
                'status'     => Event::$status['DELETED'],
                'deleted_at' => time(),
            ]);

            #redis
            if (env('CACHE_DRIVER') == 'redis') {
                $eventKeys = app('redis')->keys(env('CACHE_PREFIX') . ':events:*');
                foreach ($eventKeys as $key) {
                    app('redis')->del($key);
                }
            }

            return response()->json([
                'message' => 'رویداد باموفقیت حذف شد',
                'status'  => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'در حذف رویداد مشکلی رخ داده است. لطفا مجددا تلاش کنید',
                'status'  => 'failed'
            ], 500);
        }
    }

    /**
     * join to active event
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function join(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events'
        ]);

        try {
            $event = Event::whereId($request->input('id'))->firstOrFail();

            /* handler */
            $userHandler = new UserHandler();

            /* validation chain :D */
            $userHandler
                ->next(new StatusHandler())
                ->next(new ExpireHandler())
                ->next(new LimitHandler());

            $result = $userHandler->process($request, $event);

            if (!is_null($result)) {
                return response()->json($result, 403);
            }

            if (
                EventUser::where('user_id', $request->auth->id)->where('event_id', $event->id)->exists()
            ) {
                return response()->json([
                    'message' => 'شما عضو این رویداد می باشید',
                    'status' => 'failed',
                ], 403);
            }

            EventUser::create([
                'user_id'    => $request->auth->id,
                'event_id'   => $event->id,
                'created_at' => time()
            ]);

            $event->increment('members');

            return response()->json([
                'message' => 'شما به شرکت کنندگان این رویداد پیوستید',
                'status' => 'success',
            ], 200);
        } catch (\Throwable $th) {
            throw new \Exception($th);

            return response()->json([
                'message' => 'مشکلی در پیوستن شما به این رویداد رخ داده است',
                'status' => 'failed',
            ], 500);
        }
    }

    /**
     * leave from an event
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function leave(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events'
        ]);

        try {
            $event     = Event::whereId($request->input('id'))->firstOrFail();
            $eventUser = EventUser::where('user_id', $request->auth->id)
                ->where('event_id', $event->id)
                ->first();

            if (is_null($eventUser)) {
                return response()->json([
                    'message' => 'شما عضو این رویداد نیستید',
                    'status' => 'failed',
                ], 403);
            }

            $eventUser->delete();
            $event->decrement('members');

            return response()->json([
                'message' => 'شما با موفقیت خارج شدید',
                'status' => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در خروج شما از رویداد رخ داده است, لطفا مجددا تلاش کنید',
                'status' => 'failed',
            ], 500);
        }
    }

    /**
     * members who follow an event
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function members(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:events'
        ]);

        try {
            $event   = Event::whereId($request->input('id'))
                ->select('id')
                ->where('status', Event::$status['MEMBERING'])
                ->with('members:id,name,avatar')
                ->firstOrFail();

            return response()->json([
                'members'   => $event->members,
                'status'    => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'این رویداد فعال نیست و امکان عضوشدن و یا مشاهده شرکت کنندگان فراهم نمی باشد',
                'status'  => 'failed'
            ], 500);
        }
    }

    /**
     * change some params for show in api
     *
     * @param Event $event
     * @return array
     */
    private static function showableEvent(Event $event): array
    {
        return [
            'id' => $event->id,
            'title' => $event->title,
            'slug' => $event->slug,
            'string_id' => $event->string_id,
            'description' => $event->description,
            'image' => $event->image != null ? env('APP_URL') . "/upload/events/$event->image" : env('APP_URL') . "/static/null_event_image.png",
            'origin_city' => $event->origin_city,
            'origin_state' => $event->origin_state,
            'start_day' => Time::timestampToString($event->start_at),
            'destination_city' => $event->destination_city,
            'destination_state' => $event->destination_state,
            'max_member' => $event->max_member,
            'members' => $event->members,
            'status' => $event->status,
            'level' => $event->level
        ];
    }
}
