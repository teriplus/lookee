<?php

namespace App\Console\Commands;

use App\Models\Event;
use App\Services\Helper\Time;
use Illuminate\Console\Command;

class ExpireEvents extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'process:expire-events';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'expire events who start_day AND start_time is past';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		Event
			::select('id', 'status', 'start_day', 'start_time')
			->whereIn('status', [
				Event::$status['INIT'],
				Event::$status['MEMBERING']
			])
			->where('start_at', '<', time())
			->take(100)
			->update([
				'status' => 3, // Event::$status['FINISHED']
			]);
	}
}
