<?php

namespace App\Http\Controllers\App;

use App\Models\User;
use Illuminate\Http\Request;
use App\Services\Helper\Time;
use App\Services\Payment\Pay;
use App\Http\Controllers\Controller;

class VIPController extends Controller
{
    public function payOneMonth(Request $request)
    {
        $this->validate($request, [
            'getway' => 'required|string'
        ]);
        /**
         * Amazing !!!
         */
        Pay::getway($request->getway)
            ->setUser($request->auth)
            ->price('300000')
            ->unit('oneMonthVIP')
            ->exec();
    }

    public function payThreeMoths(Request $request)
    {
        $this->validate($request, [
            'getway' => 'required|string'
        ]);

        Pay::getway($request->getway)
            ->setUser($request->auth)
            ->price('900000')
            ->unit('threeMothsVIP')
            ->exec();
    }

    public function info(Request $request)
    {
        if ($request->auth->vip_status == User::$vip_status['INACTIVE']) {
            return response()->json([
                'status'  => 'success',
                'message' => 'شما وی ای پی نیستید'
            ]);
        }

        return response()->json([
            'status'  => 'success',
            'data'    => [
                'expire_at' => Time::daykeyToString($request->auth->vip_expire_at)
            ]
        ]);
    }
}
