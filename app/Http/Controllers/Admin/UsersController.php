<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\User;
use App\Services\ACL\ACL;
use Illuminate\Http\Request;
use App\Console\Commands\Permission;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function __construct(Request $request)
    {
        $this->check(['users'], $request->auth);
    }

    public function user(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:users'
        ]);
        try {
            $user = User::find($request->input('id'));
            return response()->json([
                'user'      => $user,
                'status'    => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در نمایش کاربر رخ داده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    public function users($page)
    {
        $paginate   = 30;

        try {
            $users = User::latest()
                ->skip(($page - 1) * $paginate)
                ->take($paginate)->get()->map(function ($user) {
                    return [
                        'id'    => $user->id,
                        'name'  => $user->name,
                        'email' => $user->email,
                        'avatar'        => $user->avatar,
                        'phone_number'  => $user->phone_number,

                        'status'                    => $user->status,
                        'access_to_comment'         => $user->access_comment,
                        'access_to_join_event'      => $user->access_join_event,
                        'access_to_create_event'    => $user->access_create_event,

                        'email_verified_at' => $user->email_verified_at,
                    ];
                });

            $totalUsers = User::all()->count();
            if ($totalUsers == 0) {
                return response()->json([
                    'message' => 'این صفحه وجود ندارد',
                    'status'  => 'failed'
                ], 404);
            }

            return response()->json([
                'users'     => $users,
                'paginate'   => (int) $paginate,
                'page'       => (int) $page,
                'pagesCount' => (int)  round($totalUsers / $paginate),
                'status'    => 'success',
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در نمایش کاربران رخ داده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    public function deleteUser(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:users'
        ]);

        try {
            $user = User::find($request->input('id'));
            if (ACL::hasAnyRoles(Permission::getRoles(), $request->auth)) {
                return response()->json([
                    'message' => 'شما قادر به حذف ادمین نیستید',
                    'status'  => 'failed'
                ], 403);
            }
            $user->status     = User::$status['DELETED'];
            $user->deleted_at = Carbon::now()->timestamp;
            $user->save();
            return response()->json([
                'message' => 'کاربر حذف شد',
                'status'  => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در حذف کاربر رخ داده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    public function userActivationHandler(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:users',
            'status' => 'required|integer'
        ]);

        try {
            $user = User::find($request->input('id'));
            $user->status = $request->status;
            switch ($request->status) {
                case User::$status["INACTIVE"]:
                    $response = [
                        'status'  => 'success',
                        'message' => 'اکانت کاربر غیر فعال گردید'
                    ];
                    break;

                case User::$status["ACTIVE"]:
                    $user->deleted_at = null;
                    $response = [
                        'status'  => 'success',
                        'message' => 'اکانت کاربر فعال گردید'
                    ];
                    break;

                case User::$status["DELETED"]:
                    $user->deleted_at = time();
                    $response = [
                        'status'  => 'success',
                        'message' => 'اکانت کاربر حذف گردید'
                    ];
                    break;

                case User::$status["PENDING"]:
                    $user->email_verified_at = null;
                    $response = [
                        'status'  => 'success',
                        'message' => 'اکانت کاربر درحالت انتظار قرار گرفت'
                    ];
                    break;
                default:
                    return response()->json([
                        'status' => 'failed',
                        'message'   => 'استاتوس کد نامعتبر است'
                    ], 403);
            }
            $user->save();
            return response()->json($response, 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در تغییر استاتوس کاربر رخ داده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    public function activeVIP(Request $request)
    {
        $this->validate($request, [
            'id'   => 'required|exists:users',
            'days' => 'required|integer',
        ]);

        try {
            $user = User::find($request->input('id'));
            $user->vip_status    = User::$vip_status['ACTIVE'];
            $user->vip_expire_at = strtotime(Carbon::now() . " +$request->days days");
            return response()->json([
                'message' => "وی آی پی تا $request->days روز آینده فعال می باشد",
                'status'  => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در فعال سازی وی آی پی رخ داده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    public function inactiveVIP(Request $request)
    {
        $this->validate($request, [
            'id'   => 'required|exists:users',
        ]);

        try {
            $user = User::find($request->input('id'));
            $user->vip_status    = User::$vip_status['INACTIVE'];
            $user->vip_expire_at =  null;
            return response()->json([
                'message' => 'وی آی پی غیر فعال شد',
                'status'  => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در غیر فعال سازی وی آی پی رخ داده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    public function commentHandler(Request $request)
    {
        $this->validate($request, [
            'id'   => 'required|exists:users',
        ]);

        try {
            $user = User::find($request->input('id'));
            $user->access_comment = !$user->access_comment;
            $user->save();
            return response()->json([
                'message' => $user->access_comment ?
                    'ثبت کامنت امکان پذیر است'
                    : 'ثبت کامنت ممکن نیست',
                'status'  => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در تغییر ثبت کامنت پیش آمده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    public function joinEventHandler(Request $request)
    {
        $this->validate($request, [
            'id'   => 'required|exists:users',
        ]);

        try {
            $user = User::find($request->input('id'));
            $user->access_join_event = !$user->access_join_event;
            $user->save();
            return response()->json([
                'message' => $user->access_join_event ?
                    'پیوستن به رویداد امکان پذیر است'
                    : 'پیوستن به رویداد ممکن نیست',
                'status'  => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در تغییر پیوستن به رویداد پیش آمده است',
                'status'  => 'failed'
            ], 500);
        }
    }

    public function createEventHandler(Request $request)
    {
        $this->validate($request, [
            'id'   => 'required|exists:users',
        ]);

        try {
            $user = User::find($request->input('id'));
            $user->access_create_event = !$user->access_create_event;
            $user->save();
            return response()->json([
                'message' => $user->access_create_event ?
                    'ساخت رویداد امکان پذیر است'
                    : 'ساخت رویداد رویداد ممکن نیست',
                'status'  => 'success'
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'message' => 'مشکلی در تغییر ساخت رویداد پیش آمده است',
                'status'  => 'failed'
            ], 500);
        }
    }
}
