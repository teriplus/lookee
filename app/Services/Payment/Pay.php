<?php

namespace App\Services\Payment;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\App\Transaction\TransactionsController;

class Pay
{
    public static $getwayClass;
    protected static $price;
    public static $days;
    public static $unit;
    protected static $user;

    public static function getway(String $getway)
    {
        self::$getwayClass = [
            'zibal'  => \App\Services\Payment\Zibal\ZibalPayment::class,
            'mellat' => \App\Services\Payment\Mellat\MellatPayment::class
        ][$getway];
        return new static;
    }

    public static function price(String $rials)
    {
        self::$price = $rials;
        return new static;
    }

    public static function unit(String $unit)
    {
        $exists = method_exists(new TransactionsController(), $unit);
        if ($exists) {
            throw new \Exception("Method Unit Not Exist In app/Http/Controllers/App/Transaction/Units");
        }
        self::$unit = $unit;
        return new static;
    }


    public static function days(String $days)
    {
        self::$days = $days;
        return new static;
    }

    public static function setUser(User $user)
    {
        self::$user = $user;
        return new static;
    }

    public static function verify(Request $request): array
    {
        $transaction = self::$getwayClass::getSupportTransaction();
        return $transaction->verify($request);
    }

    public static function exec()
    {
        $transaction = self::$getwayClass::getSupportTransaction();
        $transaction->pay([
            'user'   => self::$user,
            'amount' => self::$price,
            'unit'   => self::$unit,
        ]);
    }
}
