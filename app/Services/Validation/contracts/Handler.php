<?php

namespace App\Services\Validation\contracts;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Services\Validation\contracts\IHandle;

abstract class Handler implements IHandle
{
    private $nextHandler;

    public function next($nextHandler)
    {
        return $this->nextHandler = $nextHandler;
    }

    public function process(Request $request,Event $event)
    {
        if (isset($this->nextHandler)) {
            return $this->nextHandler->process($request, $event);
        }
        return;
    }
}
