<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupportController extends Controller
{
    public function __construct(Request $request)
    {
        $this->check(['support'], $request->auth);
    }
}
