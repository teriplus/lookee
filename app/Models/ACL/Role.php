<?php

namespace App\Models\ACL;

use App\Models\ACL\Permission;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Role extends Model
{
    // use HasFactory;
    
    protected $table = "roles";

    public $timestamps = false;

    protected $guarded = [];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
}
