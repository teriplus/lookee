<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerificationMailable;
use App\Models\User;

class SendVerificationEmail extends Job
{
    private User $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to([$this->user->email])->send(new EmailVerificationMailable($this->user));
    }
}
