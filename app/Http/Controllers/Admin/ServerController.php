<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;

class ServerController extends Controller
{
    public function __construct(Request $request)
    {
        $this->check(['server'], $request->auth);
    }

    public function artisan(Request $request)
    {
        $this->validate($request, [
            'comment'   => 'required|string',
        ]);
        // TODO Validate comment for danger comments
        try {
            Artisan::call($request->comment);
            return response()->json([
                'status'    => 'success',
                'message'   => "executed successfully",
            ], 200);
        } catch (\Throwable $th) {
            // throw new \Exception($th);
            return response()->json([
                'status'    => 'failed',
                'message'   => "some problem in executing",
            ], 403);
        }
    }

    public function redis(Request $request)
    {
        $this->validate($request, [
            'switch'    => 'required|string',
            'comment'   => 'nullable|string',
        ]);

        if (env('CACHE_DRIVER') != 'redis') {
            return response()->json([
                'status'    => 'failed',
                'message'   => "redis is not active",
            ], 403);
        }

        switch ($request->switch) {
            case "keys":
                return app('redis')->keys($request->comment);
                break;
            case "del":
                return app('redis')->del($request->comment);
                break;
            case "del_keys":
                collect(app('redis')->keys($request->comment))
                    ->each(function ($key) {
                        app('redis')->del($key);
                    });
                return 1;
                break;
            case "flushall":
                return app('redis')->flushall();
                break;
            case "help":
                return [
                    'keys' => [
                        'description' => "return an array of all keys matched 'comment' "
                    ],
                    'del' => [
                        'description' => "delete a key equals 'comment' "
                    ],
                    'del_keys' => [
                        'description' => "delete keys matched 'comment' "
                    ],
                    'flushall' => [
                        'description' => 'FLUSHALL keys'
                    ]
                ];
                break;
            default:
                return response()->json([
                    'status'    => 'failed',
                    'message'   => "Command '$request->switch' not found",
                ], 403);
        }
    }
}
