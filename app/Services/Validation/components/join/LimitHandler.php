<?php

namespace App\Services\Validation\components\join;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Services\Validation\contracts\Handler;

class LimitHandler extends Handler
{
    public function process(Request $request,Event $event)
    {
        if ($event->members >= $event->max_member) {
            return [
                'message' => 'این رویداد پرشده است',
                'status'  => 'failed'
            ];
        }

        return parent::process($request, $event);
    }
}
