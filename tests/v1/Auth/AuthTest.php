<?php

use App\Models\User;

class AuthTest extends TestCase
{
    /** @test */
    public function can_register()
    {
        $email = "test" . rand(1000, 9999) . "@test.test";
        $response = $this->json(
            'POST',
            'v1/register',
            [
                'name'     => 'test',
                'email'    => $email,
                'password' => '123',
            ]
        );
        $this->assertEquals($this->response->status(), 200);
        $this->seeInDatabase('users', ['email' => $email]);
        return User::latest()->first();
    }

    /**
     * @test 
     * @depends can_register
     */
    public function can_login(User $user)
    {
        $response = $this->json(
            'POST',
            'v1/login',
            [
                'email'    => $user->email,
                'password' => '123',
            ]
        );
        $this->assertEquals($this->response->status(), 200);
        return [
            'token' => $this->getContent($response, 'token'),
            'user'  => User::where('email', $user->email)->first()
        ];
    }

    /**
     * @test 
     * @depends can_login
     */
    public function auth_token_is_work(array $auth)
    {
        $response = $this->json(
            'POST',
            'v1/check/auth',
            [],
            [
                'token' => $auth['token']
            ]
        );
        $this->assertEquals($this->response->status(), 200);
    }
}
