<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Morilog\Jalali\Jalalian;

class PaymentsController extends Controller
{
    public function __construct(Request $request)
    {
        $this->check(['payments'], $request->auth);
    }

    public function payment(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|exists:payments'
        ]);

        try {
            $payment = Payment::with('user')->find($request->id);
            $payment->owner = $payment->user->only(User::$showable);
            unset($payment->user);

            return response()->json([
                'status' => 'success',
                'data'   => [$payment]
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'success',
                'message' => 'مشکلی در نمایش پرداخت به وجود آمده است'
            ], 500);
        }
    }

    public function payments()
    {
        try {
            $payments = Payment::with('user')->paginate(30);

            foreach ($payments as $payment) {
                $payment->owner = $payment->user->only(User::$showable);
                unset($payment->user);

                $payment->verified_at = is_null($payment->verified_at) ? null :
                    Jalalian::forge($payment->verified_at)->format(env('DATE_FORMAT', '%B %d، %Y'));
                $payment->created_at  = is_null($payment->created_at) ? null :
                    Jalalian::forge($payment->created_at)->format(env('DATE_FORMAT', '%B %d، %Y'));
            }

            return response()->json([
                'status' => 'success',
                'data'   => [$payments]
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'success',
                'message' => 'مشکلی در نمایش پرداخت ها به وجود آمده است'
            ], 500);
        }
    }
}
