<?php

use App\Models\Comment;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Comments extends Migration
{
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); 

            $table->integer('commentable_id');
            $table->string('commentable_type');
            $table->text('body');
            $table->tinyInteger('status')->default(Comment::$status['ACTIVE']);

            $table->string('created_at', 16)->nullable();
            $table->string('updated_at', 16)->nullable();
            $table->string('deleted_at', 16)->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
