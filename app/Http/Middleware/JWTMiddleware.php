<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JWTMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('token');

        if (!$token) {
            return response()->json([
                'status'  => 'failed',
                'message' => 'توکن یافت نشد',
            ], 401);
        }

        try {
            $credentials = JWT::decode($token, env('APP_KEY'), ['HS256']);
        } catch (ExpiredException $e) {
            return response()->json([
                'status'  => 'failed',
                'message' => 'توکن منقضی شده است لطفا مجددا وارد شوید',
            ], 200);
        } catch (Exception $e) {
            return response()->json([
                'status'  => 'failed',
                'messgae' => 'مشکلی در احراز هویت از طریق توکن رخ داده است',
            ], 401);
        }

        try {
            $user = User::where('id', $credentials->sub)->firstOrFail();
            switch ($user->status) {
                case User::$status["INACTIVE"]:
                    return response()->json([
                        'status'  => 'failed',
                        'message' => 'اکانت شما از طریق سیستم بسته شده است, درصورت تمایل با پشتیباتی تماس حاصل فرمایید'
                    ], 403);
                    break;

                case User::$status["ACTIVE"]:
                    $request->auth = $user;
                    return $next($request);
                    break;

                case User::$status["DELETED"]:
                    $request->auth    = $user;
                    $request->deleted = true;
                    return $next($request);
                    break;

                case User::$status["PENDING"]:
                    $request->auth = $user;
                    return $next($request);
                    break;
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status'  => 'failed',
                'message' => 'کاربر یافت نشد'
            ], 404);
        }
    }
}
