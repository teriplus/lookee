<?php

namespace App\Models\ACL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Permission extends Model
{
    // use HasFactory;
    
    protected $table = "permissions";

    public $timestamps = false;

    protected $guarded = [];
}
