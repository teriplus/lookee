# Lookee
```
lumen based project
```
### Run Server
```
php -S localhost:8000 -t public/
```
### Database
```
php artisan migrate
```
### Run Fakers
```
php artisan db:seed
```
### Setup ACL
```
php artisan make:permission --id=1 --to=super --truncate=yes
```
### Update Document
```
phpDocumentor -d . -t docs/api --ignore .vendor
```