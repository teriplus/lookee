<?php

namespace App\Http\Controllers\Admin;

use App\Services\ACL\ACL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function __construct(Request $request)
    {
        # code...
    }

    public function access(Request $request)
    {
        $permissions = [];

        foreach(ACL::userPermissions($request->auth) as $permission){
            array_push($permissions, $permission);
        }

        return response()->json([
            'access' => $permissions,
            'name'   => $request->auth->name,
            'status' => 'success'
        ], 200);
    }
}
