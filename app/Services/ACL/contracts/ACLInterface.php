<?php

namespace App\Services\ACL\contracts;

use App\Models\User;

interface ACLInterface
{
    public static function giveRoles(array $roles, User $user);
    public static function takeOverRoles(array $roles, User $user);
    public static function refreshRoles(array $roles, User $user);
    public static function hasRoles($roles, User $user): bool;
    public static function hasAnyRoles($roles, User $user): bool;
    public static function hasPermissions($assert_permissions, User $user);
    public static function userPermissions(User $user);
}
