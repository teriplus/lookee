<?php

namespace App\Http\Controllers\App;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use function PHPUnit\Framework\fileExists;

class ProfileController extends Controller
{
	public function show(Request $request)
	{
		try {
			$profile = $request->auth->only(User::$showable);
			$profile['avatar'] = self::getAvatar($profile['avatar']);
			return response()->json([
				'profile'	=> $profile,
				'status'	=> 'success'
			], 200);
		} catch (\Throwable $th) {
			return response()->json([
				'message' => 'مشکلی در بارگذرای پروفایل به وجود آمده است',
				'status'  => 'failed'
			], 500);
		}
	}

	public function update(Request $request)
	{
		$validator = Validator::make(
			$request->all(),
			[
				'name'		=> 'required',
				'birth_day'	=> 'nullable|numeric|regex:/^\d{8}$/u',
				'gender'	=> 'nullable',
				'bio'		=> 'nullable',
				'telegram_id'	=> 'nullable',
				'instagram_id'	=> 'nullable',
			]
		);

		if ($validator->fails()) {
			return response()->json([
				'status'  => 'failed',
				'message' => $validator->errors()->first()
			], 422);
		}

		try {
			User::findOrFail($request->auth->id)
				->update($request->only(User::$updatable));

			return response()->json([
				'message' => 'پروفایل باموفقیت به روزرسانی شد',
				'status' => 'success'
			], 200);
		} catch (\Throwable $th) {
			return response()->json([
				'message' => 'مشکلی در روزرسانی پروفایل به وجود آمده است',
				'status' => 'failed'
			], 500);
		}
	}

	public function setPhoneNumber(Request $request)
	{
		$this->validate($request, [
			'phone_number' => 'required|unique:users'
		]);

		if (!preg_match('/9(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}/', $request->input('phone_number'))) {
			return response()->json([
				'message' => 'شماره در فرمت درست وارد نشده است',
				'example' => '9123456789',
				'status'  => 'failed'
			], 400);
		};

		try {
			$request->auth->phone_number = $request->input('phone_number');
			$request->auth->save();

			return response()->json([
				'message' => 'شماره با موفقیت ثبت شد',
				'status' => 'success'
			], 200);
		} catch (\Throwable $th) {
			return response()->json([
				'message' => 'مشکلی در ثبت شماره به وجود آمده است',
				'status' => 'failed'
			], 500);
		}
	}

	public function setAvatar(Request $request)
	{
		$this->validate($request, [
			'avatar' => 'required|image|mimes:jpeg,png,jpg|max:1024'
		]);

		try {
			$avatar    = $request->file('avatar');
			$oldAvatar = $request->auth->avatar ?? null;

			$path 	  = $avatar->store('upload/avatars');
			$name 	  = explode(DIRECTORY_SEPARATOR, $path)[2];

			$request->auth->avatar = $name;
			$request->auth->save();

			if (!is_null($oldAvatar)) {
				$purePath = realpath(__DIR__ . "/../../../../storage/app/upload/avatars/{$oldAvatar}");
				if (fileExists($purePath)) {
					unlink($purePath);
				}
			}

			return response()->json([
				'message' => 'آواتار با موفقیت بارگذاری شد',
				'status' => 'success'
			], 200);
		} catch (\Throwable $th) {
			return response()->json([
				'message' => 'مشکلی در بارگذاری آواتار رخ داده است',
				'status' => 'failed'
			], 500);
		}
	}

	public static function getAvatar($name)
	{
		if (is_null($name)) {
			return null;
		}

		return "upload/avatars/{$name}";
	}

	public function deleteAvatar(Request $request)
	{
		if ($request->auth->avatar == null) {
			return response()->json([
				'message' => 'هیچ عکسی برای حذف وجود ندارد',
				'status'  => 'failed'
			], 400);
		}

		try {
			$purePath = realpath(__DIR__ . "/../../../../storage/app/upload/avatars/{$request->auth->avatar}");
			if (fileExists($purePath)) {
				unlink($purePath);
				$request->auth->avatar = null;
				$request->auth->save();
				return response()->json([
					'message' => 'آواتار با موفقیت حذف شد',
					'status'  => 'success',
				], 200);
			}
		} catch (\Throwable $th) {
			return response()->json([
				'message' => 'آواتار حذف نشد',
				'status'  => 'failed',
			], 500);
		}
	}
}
